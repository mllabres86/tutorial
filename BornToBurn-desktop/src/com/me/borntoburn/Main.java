package com.me.borntoburn;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "BornToBurn";
		cfg.useGL20 = false;
		
		//tama�o de la pantalla
		//cfg.width = 480;
		//cfg.height = 320;
		
		cfg.width = 780;
		cfg.height = 520;
		
		//cfg.width = 960;
		//cfg.height = 640;

		new LwjglApplication(new Drop(), cfg);
	}
}
