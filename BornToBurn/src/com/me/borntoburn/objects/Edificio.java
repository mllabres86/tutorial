package com.me.borntoburn.objects;

import java.util.ArrayList;

import com.me.borntoburn.screens.BornToBurn;


public class Edificio {
	public Casilla casillaInicial;
	public int casillasAncho;
	public int casillasAlto;
	public boolean quemable;
	public boolean quemado=false;
	public boolean rodeado=false;

	public ArrayList<Casilla> adyacentes = new ArrayList<Casilla>();
	public ArrayList<Casilla> propias = new ArrayList<Casilla>();

	public Edificio(Casilla casillaInicial, int casillasAncho,int casillasAlto, boolean quemable) {
		super();
		this.casillaInicial = casillaInicial;
		this.casillasAlto = casillasAlto;
		this.casillasAncho = casillasAncho;
		this.quemable = quemable;

		for(int i = casillaInicial.columna ; i< casillaInicial.columna+casillasAncho;i++){
			for(int j = casillaInicial.fila ; j < casillaInicial.fila+casillasAlto;j++){
				propias.add(BornToBurn.getMyCas(j, i));
				BornToBurn.getMyCas(j, i).ocupado=true;
			}
		}
		//getAdy();

	}

	public void getAdy() {
		adyacentes.clear();
		for (int i = -1; i < this.casillasAncho + 1; i++) {
			for (int j = -1; j < this.casillasAlto + 1; j++) {
				try{
				if(BornToBurn.getMyCas(casillaInicial.fila+j, casillaInicial.columna + i).columna>=0 &&
						BornToBurn.getMyCas(casillaInicial.fila+j, casillaInicial.columna + i).fila>=0 ){
					if(!BornToBurn.getMyCas(casillaInicial.fila+j, casillaInicial.columna + i).ocupado){
						adyacentes.add(BornToBurn.getMyCas(casillaInicial.fila+j, casillaInicial.columna + i));
					}
			}
				}catch(NullPointerException e){
					//System.out.println("e getAdy()");
				}
			}
		}
		adyacentes.removeAll(propias);
	}

	public int countAdy() {
		
		for(int i = 0 ; i<adyacentes.size();i++){
			try{
			System.out.println(Integer.toString(adyacentes.get(i).fila)+","+Integer.toString(adyacentes.get(i).columna));
			}catch(NullPointerException e){
				System.out.println("e countAdy()");
			}
		}
		
		return adyacentes.size();
	}
	
	/**
	 * Comprueba las adyacentes. Devuelve true si estan manifestadas
	 * @return
	 */
	public boolean isRodeado(){
		boolean result= true;
			for (int j = 0; j <adyacentes.size(); j++) {//por cada quemable recorremos sus adyacentes
				if(!adyacentes.get(j).manifestado){
					result=false;
				}
			}
	
		rodeado=result;
		return rodeado;
	}
}
