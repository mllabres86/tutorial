package com.me.borntoburn.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class Heavy extends Personaje {
	//public int numManif = 0;
	public String direccion="down",lastDir="down";
	boolean leftFoot=true;
	//public double movingTime;
	public static Texture[] heavy = {
		new Texture(Gdx.files.internal("data/img/heavy/heavyF.png")),
		new Texture(Gdx.files.internal("data/img/heavy/heavyB.png")),
		new Texture(Gdx.files.internal("data/img/heavy/heavyFL.png")),
		new Texture(Gdx.files.internal("data/img/heavy/heavyFR.png")),
		new Texture(Gdx.files.internal("data/img/heavy/heavyBL.png")),
		new Texture(Gdx.files.internal("data/img/heavy/heavyBR.png"))
	};


	public Heavy(Texture texturaPj, Casilla casilla) {
		super(texturaPj, casilla);
		movingTime=0.45;
	}

	public Heavy(Casilla casilla) {
		super(heavy[0], casilla);
		movingTime=0.45;
	}

	public Texture texturaCaminar() {
		Texture retornable=null;
		if(direccion.equalsIgnoreCase("down")){
			lastDir="down";
			if(leftFoot){
				retornable=heavy[2];
				leftFoot=false;
			}else{
				retornable=heavy[3];
				leftFoot=true;
			}
		}else if(direccion.equalsIgnoreCase("up")){
			lastDir="up";

			if(leftFoot){
				retornable=heavy[4];
				leftFoot=false;
			}else{
				retornable=heavy[5];
				leftFoot=true;
			}
		}else if(direccion.equalsIgnoreCase("left")){
			lastDir="left";

			if(leftFoot){
				retornable=heavy[2];
				leftFoot=false;
			}else{
				retornable=heavy[3];
				leftFoot=true;
			}
		}else if(direccion.equalsIgnoreCase("right")){
			lastDir="right";

			if(leftFoot){
				retornable=heavy[2];
				leftFoot=false;
			}else{
				retornable=heavy[3];
				leftFoot=true;
			}
		}
		return retornable;
	}
	public Texture texturaStop() {
		Texture retornable=null;
		if(lastDir.equalsIgnoreCase("down")){
			retornable=heavy[0];
		}else if(lastDir.equalsIgnoreCase("up")){
			retornable=heavy[1];
		}else if(lastDir.equalsIgnoreCase("left")){
			retornable=heavy[0];
		}else if(lastDir.equalsIgnoreCase("right")){
			retornable=heavy[0];
		}
		return retornable;
	}

}
