package com.me.borntoburn.objects;


public class Casilla {
	public int fila;
	public int columna;
	public float pixelX;
	public float pixelY;
	float tamano;
	public boolean ocupado=false;
	public boolean manifestado=false;
	
	
//	public Casilla(int columna, int fila){
//		this.fila=fila;
//		this.columna=columna;
//	}
	public Casilla(int fila, int columna, float pixelX, float pixelY,float tamano, boolean ocupado) {
		super();
		this.fila = fila;
		this.columna = columna;
		this.pixelX = pixelX;
		this.pixelY = pixelY;
		this.tamano = tamano;
	}

	public void ocupar(){
		ocupado=true;
	}
	
	public void desalojar(){
		ocupado=false;
	}
	public String tostring(){
		return columna+","+fila;
	}
}
