package com.me.borntoburn.objects;

import java.util.Hashtable;
import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.me.borntoburn.screens.BornToBurn;

public class Personaje {
	public Texture texturaPj;
	public Casilla casilla;
	public double movingTime=0.5;
	boolean verbose=false;
	String direccion="";
	Casilla noWay;
	public Hashtable<Casilla, String> dirs;
	int [] max;
	public Personaje(Texture texturaPj, Casilla casilla) {
		this.texturaPj = texturaPj;
		this.casilla = casilla;
		dirs = new Hashtable<Casilla, String>();
		max = BornToBurn.lados();
		noWay=casilla;
		alrededor(casilla);
	}

	
	/**
	 * comprueba sus casillas superior, inferior y las dos laterales.
	 * Las que son practicables las guarda como posibles direcciones.
	 * Posibilidad de entrar parametros de direccion prohibida
	 * @param prohibida direcion o direcciones prohibidas EJ:"left","right"
	 * @return un array de posibles direcciones
	 */
	public void alrededor(Casilla casilla, Casilla... prohibida){
		dirs.clear();
		if(casilla.columna+1< max[1]){
			if(!BornToBurn.getMyCas(casilla.fila,casilla.columna+1).ocupado){
				//DERECHA
					 dirs.put((BornToBurn.getMyCas(casilla.fila,casilla.columna+1)),"right");
			}
		}
		if(casilla.columna-1 >= 0){
			if(!BornToBurn.getMyCas(casilla.fila,casilla.columna-1).ocupado){
				//IZQUIERDA
					dirs.put(BornToBurn.getMyCas(casilla.fila,casilla.columna-1),"left");	
			}
		}
		if(casilla.fila+1 < max[0]){
			if(!BornToBurn.getMyCas(casilla.fila+1,casilla.columna).ocupado  ){
				//ARRIBA
					dirs.put(BornToBurn.getMyCas(casilla.fila+1,casilla.columna),"up");
			}
		}
		if(casilla.fila-1 >= 0){
			if(!BornToBurn.getMyCas(casilla.fila-1,casilla.columna).ocupado ){
				//ABAJO
					dirs.put(BornToBurn.getMyCas(casilla.fila-1,casilla.columna),"down");
			}
		}
		if(prohibida.length>0){
			for(int i = 0 ; i<prohibida.length;i++){
				if(verbose){
					System.out.println("prohibida "+prohibida[0].columna+","+prohibida[0].fila);
				}
				if(dirs.containsKey(prohibida[i])){
					if(verbose){
						System.out.println("tenemos "+dirs.size()+"\nborrada "+prohibida[i].columna+","+prohibida[i].fila);
					}
					dirs.remove(prohibida[i]);
					if(verbose){
						System.out.println("ahora tenemos "+dirs.size());
					}
				}
			}
			if(verbose){
				System.out.println("-----------------------");
			}
		}
	}
	
	
	/**
	 * @param direcciones hashtable que relaciona una casillacon la direccion,
	 * @param recomendadas casillas preferentes
	 * @return una unica casilla
	 */
	public Casilla decideDireccion(Hashtable<Casilla, String> direcciones, Casilla[] ...recomendadas){
		if(recomendadas.length>0){
			//System.out.println("recom "+recomendadas[0][0]);
			for(int i = 0 ; i < recomendadas[0].length ; i ++){
				if(direcciones.containsKey(recomendadas[0][i])){
					return recomendadas[0][i];
				}
			}
		}
		Object[] casillas =  direcciones.keySet().toArray();
		Object casilla = casillas[new Random().nextInt(casillas.length)];
		noWay=this.casilla;
		return  (Casilla) casilla;
	}

	
	/**
	 * Se mueve hasta encontrarse en un cruce donde puede seguir recto o cambiar la direccion.
	 * Utiliza liearVision.
	 * EJ: recorrera la calle y cambiara la direccion en un cruce.
	 */
	public void cautionMove(){
		casilla=decideDireccion(dirs);
		direccion=dirs.get(casilla);
		alrededor(casilla,noWay);
		if(verbose){
			System.out.println("movido a "+casilla.columna+","+casilla.fila);
		}
		//linearVision(3);
	}
}
