package com.me.borntoburn.objects;


import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.me.borntoburn.Drop;
import com.me.borntoburn.screens.BornToBurn;

/**
 * Funcionamiento del madero:
 * se mueve
 * a�ade las casillas en su campode vision
 * comprueba si hay manifestante en sus casillas de campo de vision
 * si los hay los persigue
 * cuando atrapa a uno lo detiene y renace en la comisaria
 * @author mikel
 *
 */
public class Madero extends Personaje {
	long puntuacion;
	String mode;
	boolean verbose=false;
	public Manifestacion manifa;
	Casilla inicial;
	//String direccion;
	//Casilla noWay;
	//public Hashtable<Casilla, String> dirs;
	public ArrayList<Casilla> objDirs;
	ArrayList<String> objetivoLoc;
	//int [] max;
	public ArrayList<Casilla> campoVision;
	static Texture[] texturas = {
		new Texture(Gdx.files.internal("data/img/polis/policia.png")),
		new Texture(Gdx.files.internal("data/img/polis/policia2.png")),
		new Texture(Gdx.files.internal("data/img/polis/urbanaUp.png")),
		new Texture(Gdx.files.internal("data/img/polis/urbanaDown.png")),
		new Texture(Gdx.files.internal("data/img/polis/urbanaLeft.png")),
		new Texture(Gdx.files.internal("data/img/polis/urbanaRight.png"))
	};
	
	/**
	 * Constructor de madero.
	 * @param casilla casilla inicial.
	 */
	public Madero(Casilla casilla) {
		super(texturas[0], casilla);
		movingTime=1;
		inicial=casilla;
		//max = BornToBurn.lados();
		mode="relax";
		//direccion="";
		//noWay=casilla;
		objDirs = new ArrayList<Casilla>();
		objetivoLoc = new ArrayList<String>();
		campoVision = new ArrayList<Casilla>();
		//alrededor(inicial);
	}
	
	/**
	 * Durante el servicio un policia tiene 3 modos: relax, atento y alerta y un comportamiento para cada modo.
	 * @param manifa
	 */
	public void servicio(Manifestacion manifa){
		this.manifa=manifa;
		if(mode.equalsIgnoreCase("relax")){
			texturaPj=texturas[0];
			cautionMove();
			linearVision(3);
			//crosVision(3);
			comprobarMani();
		}else{
			perseguir(manifa.get(0));
		}
	}
	
	/**
	 * compara su casilla con la de todos los manifestantes y si coinciden
	 * transforma el manifestante en dummie. El policia renace en su casilla
	 * inicial y su mode cambia a relax
	 */
	public void arrestar(){
//		if(casilla==manifa.get(0).casilla){
//			//TODO GAME OVER
//		}else{
			for(int i = manifa.size()-1 ; i > 0;i--){
				if(casilla==manifa.get(i).casilla){
					manifa.get(i).texturaPj = BornToBurn.textDummie;
					BornToBurn.dummies.add(manifa.get(i));
					BornToBurn.manifa.remove(manifa.get(i));
					//casilla=inicial;
					//noWay=inicial;
					mode="relax";
					alrededor(inicial);
				}
			}
		//}
	}
	
	
	/**
	 * selecciona un unico valor de una lista de strings
	 * @param direcciones listado de valores entrecomillados y separados por comas. EJ:"up","down"
	 * @return un unico valor
	 */
	public String listChooser(String... direcciones){
		String[] dires = new String[direcciones.length];
		String proximadireccion="";
		for(int i = 0 ; i <direcciones.length;i++){
			dires[i]=direcciones[i];
		}
		proximadireccion=(dires[new Random().nextInt(dires.length)]);
		return proximadireccion;
	}
	
	
	
	/**
	 * Se mueve hasta chocar con una casilla ocupada o hasta los bordes del mapa.
	 * Utiliza crosVision
	 * EJ: recorrera la calle y cambiara la direccion al terminarse.
	 */
	public void relaxMove(){
		switch (direccion) {
		case "right": //DERECHA
			if((casilla.columna+1< max[1])){
				Casilla rightCas = BornToBurn.getMyCas(casilla.fila,casilla.columna+1);
				if(!rightCas.ocupado){
					this.casilla=rightCas;
				}else{			
					direccion =	listChooser("up","down","left");
					break;
				}
			}else{			
				direccion = listChooser("up","down","left");
				break;
			}
			break;
		case "up": //ARRIBA
			if((casilla.fila+1 < max[0])){
				Casilla upCas = BornToBurn.getMyCas(casilla.fila+1,casilla.columna);
				if(!upCas.ocupado  ){
					this.casilla=upCas;
				}else{			
					direccion = listChooser("right","down","left");
					break;
				}
			}else{			
				direccion = listChooser("right","down","left");
				break;
			}
			break;
		case "left": //IZQUEIRDA
			if((casilla.columna-1 >= 0)){
				Casilla leftCas = BornToBurn.getMyCas(casilla.fila,casilla.columna-1);
				if(!leftCas.ocupado){
					this.casilla=leftCas;
				}else{			
					direccion = listChooser("up","down","right");
					break;
				}
			}else{
				direccion = listChooser("up","down","right");
				break;
			}
			break;
		case "down": //ABAJO
			if((casilla.fila-1 >= 0)){
				Casilla downCas = BornToBurn.getMyCas(casilla.fila-1,casilla.columna);
				if(!downCas.ocupado ){
					this.casilla=downCas;
					break;
				}else{			
					direccion = listChooser("up","right","left");
					break;
				}
			}else{			
				direccion = listChooser("up","right","left");
				break;
			}
		default:
			direccion = listChooser("up","right","left","down");

			break;
		}

		crosVision(3);
	}
	
	
	
	/**
	 * Selecciona todas las casillas no ocupadas dentro de un radio concreto EN TODAS DIRECCIONES
	 * @param radio numero de casillas que el policia ve en todas direcciones
	 */
	public void areaVision(int radio){
		campoVision.clear();
		int menosRadio=radio*-1;
		for(int i = menosRadio ; i<radio;i++){
			for(int j = menosRadio ; j<radio;j++){
				try{
					if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna + i).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna + i));
					}
				}catch(Exception e){}
			}
		}
		campoVision.remove(casilla);
	}
	
	/**
	 * Selecciona todas las casillas no ocupadas dentro de un radio concreto POR DELANTE Y POR DETRAS
	 * @param radio numero de casillas que el policia ve por delate y por detras
	 */
	public void linearVision(int radio){
		campoVision.clear();
		int menosRadio=radio*-1;
		if(direccion.equalsIgnoreCase("left") || direccion.equalsIgnoreCase("right")){
			for(int i = 1 ; i<=radio;i++){
				try{
					if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
					}else{break;}
				}catch(Exception e){}
			}
			for(int i = -1 ; i>=menosRadio;i--){
				try{
					if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
					}else{break;}
				}catch(Exception e){}
			}
		}else if(direccion.equalsIgnoreCase("up") || direccion.equalsIgnoreCase("down")){
			
			for(int j = 1 ; j<=radio;j++){
				try{
					if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
					}else{break;}
				}catch(Exception e){}
			}
			for(int j = -1 ; j>=menosRadio;j--){
				try{
					if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
					}else{break;}
				}catch(Exception e){}
			}
		}
		campoVision.remove(casilla);
	}
	
	/**
	  Selecciona todas las casillas no ocupadas dentro de un radio concreto POR DELANTE
	 * @param radio numero de casillas que el policia ve por delate
	 */
	public void frontVision(int radio){

		campoVision.clear();
		int menosRadio=radio*-1;
		if(direccion.equalsIgnoreCase("right")){
			for(int i = 1 ; i<=radio;i++){
				try{
					if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
					}else{break;}
				}catch(Exception e){}
			}
		}else if(direccion.equalsIgnoreCase("left")){
			for(int i = -1 ; i>=menosRadio;i--){
				try{
					if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
					}else{break;}
				}catch(Exception e){}
			}
		}else if(direccion.equalsIgnoreCase("up")){
			for(int j = 1 ; j<=radio;j++){
				try{
					if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
					}else{break;}
				}catch(Exception e){}
			}
		}else if(direccion.equalsIgnoreCase("down")){

			for(int j = -1 ; j>=menosRadio;j--){
				try{
					if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
						campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
					}else{break;}
				}catch(Exception e){}
			}
		}
		campoVision.remove(casilla);
	}
	
	/**
	 * Selecciona todas las casillas no ocupadas dentro de un radio concreto EN CRUZ
	 * @param radio radio numero de casillas que el policia vepor delante, detras, arriba y abajo
	 */
	public void crosVision(int radio){
		campoVision.clear();
		int menosRadio=radio*-1;
		for(int i = 1 ; i<=radio;i++){
			try{
				if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
					campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
				}else{break;}
			}catch(Exception e){}
		}
		for(int i = -1 ; i>=menosRadio;i--){
			try{
				if(!BornToBurn.getMyCas(casilla.fila, casilla.columna+i).ocupado){
					campoVision.add(BornToBurn.getMyCas(casilla.fila, casilla.columna+i));
				}else{break;}
			}catch(Exception e){}
		}
		for(int j = 1 ; j<=radio;j++){
			try{
				if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
					campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
				}else{break;}
			}catch(Exception e){}
		}
		for(int j = -1 ; j>=menosRadio;j--){
			try{
				if(!BornToBurn.getMyCas(casilla.fila+j, casilla.columna).ocupado){
					campoVision.add(BornToBurn.getMyCas(casilla.fila+j, casilla.columna));
				}else{break;}
			}catch(Exception e){}
		}
		campoVision.remove(casilla);
	}
	
	/**
	 * Comprueba si hay algun manifestante dentro de su campo de vision
	 */
	public void comprobarMani(){
		for(int i = 0 ; i< manifa.size();i++){
			if(campoVision.contains(manifa.get(i).casilla)){
				//objetivo=manifa.get(i);
				mode="alerta";
				texturaPj=texturas[1];
			}
		}
	}



	/**
	 * Persigue al objetivo
	 * @param objetivo Personaje al que perseguira
	 */
	public void perseguir(Personaje objetivo){
		objetivoLoc.clear();
		objDirs.clear();
		campoVision.clear();
		
		if(casilla.columna<objetivo.casilla.columna){
			objetivoLoc.add("right");
			if(casilla.columna+1< max[1]){
				if(!BornToBurn.getMyCas(casilla.fila,casilla.columna+1).ocupado){
					//DERECHA
						objDirs.add(BornToBurn.getMyCas(casilla.fila,casilla.columna+1));
					
				}
			}
		}
		if(casilla.columna>objetivo.casilla.columna){
			objetivoLoc.add("left");
			if(casilla.columna-1 >= 0){
				if(!BornToBurn.getMyCas(casilla.fila,casilla.columna-1).ocupado){
					//IZQUIERDA
						objDirs.add(BornToBurn.getMyCas(casilla.fila,casilla.columna-1));
					
				}
			}

		}
		if(casilla.fila<objetivo.casilla.fila){
			objetivoLoc.add("up");
			if(casilla.fila+1 < max[0]){
				if(!BornToBurn.getMyCas(casilla.fila+1,casilla.columna).ocupado  ){
					//ARRIBA
						objDirs.add(BornToBurn.getMyCas(casilla.fila+1,casilla.columna));
					
				}
			}

		}
		if(casilla.fila>objetivo.casilla.fila){
			objetivoLoc.add("down");
			if(casilla.fila-1 >= 0){
				if(!BornToBurn.getMyCas(casilla.fila-1,casilla.columna).ocupado ){
					//ABAJO
						objDirs.add(BornToBurn.getMyCas(casilla.fila-1,casilla.columna));
					
				}
			}

		}
		System.out.println(objDirs.size());
		Casilla[] locObjetivo = objDirs.toArray(new Casilla[objDirs.size()]);
		System.out.println(locObjetivo.length);
		casilla =	decideDireccion(dirs,locObjetivo);
		direccion=dirs.get(casilla);
		if(direccion.equalsIgnoreCase("up")){
			texturaPj=texturas[2];
		}else if(direccion.equalsIgnoreCase("down")){
			texturaPj=texturas[3];
		}else if(direccion.equalsIgnoreCase("left")){
			texturaPj=texturas[4];
		}else if(direccion.equalsIgnoreCase("right")){
			texturaPj=texturas[5];
		}
		alrededor(casilla,noWay);
		arrestar();

	}
	
}
