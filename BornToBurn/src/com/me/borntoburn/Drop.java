package com.me.borntoburn;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.me.borntoburn.screens.SplashScreen;

public class Drop extends Game {
	public float alto;
	public float ancho;
	public SpriteBatch batch;
	public BitmapFont font;
	public AssetManager manager;
	public long puntuacion;

	public void create() {
		// Screen
		puntuacion=0;
		ancho = Gdx.graphics.getWidth();
		alto = Gdx.graphics.getHeight();
		//distancia de la camara: cuanto mas grande mas lejos
		
		//ancho=480;
		//alto=320;
		
		ancho=780;
		alto=520;
		
		//ancho=2048;
		//alto=1600;
		
		batch = new SpriteBatch();
		font = new BitmapFont();
		manager = new AssetManager();
		this.setScreen(new SplashScreen(this));
	}

	public void render() {
		super.render();
	}

	public void dispose() {
		batch.dispose();
		font.dispose();
	}
	public void sumaPuntos(int puntos){
		if(puntos<0){
			puntuacion -= puntos;
		}else{
			puntuacion += puntos;
		}
		System.out.println(puntuacion);
	}
	
	public void quitaPuntos(int puntos){
		if(puntos>0){
			puntuacion -= puntos;
		}else{
			puntuacion += puntos;
		}
		System.out.println(puntuacion);
	}

}
