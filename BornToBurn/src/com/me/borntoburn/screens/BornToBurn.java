package com.me.borntoburn.screens;

import java.util.ArrayList;
import java.util.Enumeration;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.me.borntoburn.Drop;
import com.me.borntoburn.objects.Casilla;
import com.me.borntoburn.objects.Edificio;
import com.me.borntoburn.objects.Heavy;
import com.me.borntoburn.objects.Manifestacion;
import com.me.borntoburn.objects.Personaje;
import com.me.borntoburn.objects.Madero;

public class BornToBurn implements Screen {
	public static Drop game;
	private SpriteBatch batch;
	private Texture texture;
	OrthographicCamera camera;
	// imagenes
	//Texture textLider;
	public static Texture textDummie;
	Texture textFollower;
	Texture textMapa;
	Texture textFire;
	Texture movimiento;
	Texture filtro;
	//Texture textPoli;
	Texture textPoliVision;
	Texture textPrado;
	// Screen
	float ancho,alto,tamano;
	// Timing
	float contadorTiempoLider = 0,contadorTiempoDummies = 0,contadorTiempoPolis = 0,contadorTiempoStop = 0,contadorTransicion=0,extraH=0,extraV=0;
	//medidas
	static int filas,columnas;
	//lider
	Heavy lider;
	//arays
	public static Manifestacion manifa;
	public static ArrayList<Personaje> dummies;
	ArrayList<Madero> polis;
	ArrayList<Edificio> edificios,quemables,ardiendo;
	//metros
	Casilla l1a,l1b,l2a,l2b,l3a,l3b;
	//mapa
	private static Casilla[][] mapa;
	//comisarias
	Edificio comisaria1,comisaria2,comisaria3;
	
	//movimiento booleano
	boolean moved = false;
	//pausa booleano
	boolean paused=false;
	
	Casilla auxCasilla;
	
	boolean transX=false,transY=false;
	int trans=0;
	public BornToBurn(final Drop game) {
		//VARIABLES DEL JUEGO
		this.game = game;
		ancho = game.ancho;
		alto = game.alto;
		//INICIALIZAMOS arrays
		dummies = new ArrayList<Personaje>();
		polis = new ArrayList<Madero>();
		edificios = new ArrayList<Edificio>();
		quemables = new ArrayList<Edificio>();
		ardiendo = new ArrayList<Edificio>();
		
		//tama�o en filas y columnas
		columnas = 32;
		filas = 32;
		mapa = new Casilla[filas][columnas];
		float pixelX = -game.ancho / 2;
		float pixelY = -game.alto / 2;
		tamano = 32;
		
		

		// Crear mapa de manera l�gica
		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				mapa[j][i] = new Casilla(i, j, pixelX, pixelY, tamano, false);

				pixelX += tamano;
			}
			pixelX = -game.ancho / 2;
			pixelY += tamano;
		}

		// imagenes del juego
		//textLider = game.manager.get("data/img/heavy.png", Texture.class);
		textDummie = game.manager.get("data/img/people.png", Texture.class);
		textFollower = new Texture(Gdx.files.internal("data/img/followers.png"));
		textPoliVision = new Texture(Gdx.files.internal("data/img/polis/poliZone2.png"));
		textPrado = game.manager.get("data/img/prado.png", Texture.class);
		textFire = new Texture(Gdx.files.internal("data/img/fire.png"));
		movimiento = new Texture(Gdx.files.internal("data/img/rojo.png"));
		filtro = new Texture(Gdx.files.internal("data/img/filtros/azul37.png"));
		

		lider = new Heavy(mapa[0][0]);
		manifa = new Manifestacion(lider);
		manifa.add(new Personaje(textDummie, mapa[1][0]));
		manifa.add(new Personaje(textDummie, mapa[0][1]));
		manifa.add(new Personaje(textDummie, mapa[1][1]));
		agregaEdificios();
		manifestaCasillas();
		agregaDummies(18);
		//agregaPolis(1);
		agregaPolis(6);
		
		
		l1a = mapa[4][1];
		l1b = mapa[22][28];
		l2a = mapa[3][27];
		l2b = mapa[19][14];
		l3a = mapa[30][25];
		l3b = mapa[4][12];
		
		

		// Hide cursor
		Gdx.input.setCursorCatched(false);
		//camera
		int camAncho= (int) (ancho*1);
		int camAlto=(int) (alto*1);
		camera = new OrthographicCamera(camAncho, camAlto);
	}

	@Override
	public void dispose() {
		batch.dispose();
		texture.dispose();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
		System.out.println("game paused");
		Gdx.graphics.setContinuousRendering(false);
		Gdx.graphics.requestRendering();
	}

	@Override
	public void resume() {
		System.out.println("game resumed");
	}
	/**
	 * Actualiza lso graficos 
	 */
	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		game.batch.setProjectionMatrix(camera.combined);
		game.batch.begin();

		// Sumamos tiempo de movimiento
		contadorTiempoLider += delta;
		contadorTiempoDummies += delta;
		contadorTiempoPolis += delta;
		contadorTransicion += delta;
		
//		if (contadorTransicion > lider.movingTime/10) {
//			if(transX){
//				extraH+=3.2;
//			}
//			if(tr){
//				extraV+=3.2;
//			}
//			contadorTransicion=0;
//		}
		
		if (contadorTiempoLider > lider.movingTime) {
			moved = false;
			auxCasilla = null;
			flechasListener();
			teclasListener();
			if (moved) {
				lider.texturaPj=lider.texturaCaminar();
				cogeMetro();
				mueveMani();
				contadorTiempoLider = 0;
				manifestaCasillas();
				extraH=0;
				extraV=0;
				
			}else{
				lider.texturaPj=lider.texturaStop();
			}
		}

		// mueve todos los dummies
		if(dummies.size()>0){
			if (contadorTiempoDummies > dummies.get(0).movingTime) {
				for (int dum = 0; dum < dummies.size(); dum++) {
					dummies.get(dum).cautionMove();
				}
				contadorTiempoDummies = 0;
			}
		}
		polis.get(0);
		// mueve todos los polis
		if (contadorTiempoPolis > polis.get(0).movingTime) {
			for (int dum = 0; dum < polis.size(); dum++) {
				polis.get(dum).servicio(manifa);;				
			}
			contadorTiempoPolis = 0;
		}
		//a�ade un dummie a la manifa
		nuevoManifestante();
		
		
		
		// QUEMABLES
		compruebaQuemables();

		moveCamera(manifa.get(0).casilla.pixelX, manifa.get(0).casilla.pixelY);
		camera.update();
		
		// Dibuja fondo
		if(game.ancho==480){
			game.batch.draw(textPrado, (float)(-ancho * 1.567) ,(float)(-alto * 2.1), 2048, 2048);//480X320
		}else if(game.ancho==780){
			game.batch.draw(textPrado, (float) (-ancho *1.157),(float) (-alto * 1.485), 2048, 2048);//780X520
		}else if(game.ancho==960){
			game.batch.draw(textPrado, (float) (-ancho *1.034),(float) (-alto * 1.302), 2048, 2048);//960X640
		}else if(game.ancho==2048){
			game.batch.draw(textPrado, (float) (-ancho *0.75),(float) (-alto * 0.82), 2048, 2048);//960X640
		}
		
		if(game.ancho==480){
			game.batch.draw(filtro, (float)(-ancho * 1.567) ,(float)(-alto * 2.1), 2048, 2048);//480X320
		}else if(game.ancho==780){
			game.batch.draw(filtro, (float) (-ancho *1.157),(float) (-alto * 1.485), 2048, 2048);//780X520
		}else if(game.ancho==960){
			game.batch.draw(filtro, (float) (-ancho *1.034),(float) (-alto * 1.302), 2048, 2048);//960X640
		}else if(game.ancho==2048){
			game.batch.draw(filtro, (float) (-ancho *0.75),(float) (-alto * 0.82), 2048, 2048);//960X640
		}
		
		//pinta el lider
//		game.batch.draw(manifa.get(0).texturaPj, manifa.get(0).casilla.pixelX,
//				manifa.get(0).casilla.pixelY, 32, 32);
		game.batch.draw(lider.texturaPj, lider.casilla.pixelX+extraH,
				lider.casilla.pixelY+extraV, 32, 32);
		
		// pinta los manifestantes
		for (int i = 1; i < manifa.size(); i++) {
			game.batch.draw(manifa.get(i).texturaPj,
					manifa.get(i).casilla.pixelX, manifa.get(i).casilla.pixelY,
					32, 32);
		}

		// pinta todos los dummies
		for (int dum = 0; dum < dummies.size(); dum++) {
			game.batch.draw(dummies.get(dum).texturaPj,
					dummies.get(dum).casilla.pixelX,
					dummies.get(dum).casilla.pixelY, 32, 32);
		}
		// pinta todos los polis
		for (int dum = 0; dum < polis.size(); dum++) {
			//poliMov(polis.get(dum));
			poliVision(polis.get(dum));

			game.batch.draw(polis.get(dum).texturaPj,
					polis.get(dum).casilla.pixelX,
					polis.get(dum).casilla.pixelY, 32, 32);
		}

		
		pintaQuemados();
		
		

		
		game.batch.end();
		

	}
	
	/**
	 * pinta los edificios quemados
	 */
	public void pintaQuemados(){
		for (int i = 0; i < ardiendo.size(); i++) {
			game.batch.draw(
					textFire,
					ardiendo.get(i).casillaInicial.pixelX,
					ardiendo.get(i).casillaInicial.pixelY
					+ ((ardiendo.get(i).casillasAlto - 1) * 32),
					32 * (ardiendo.get(i).casillasAncho), 32);
		}
	}
	
	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}
	/**
	 * Actualiza la posicion de la camaraque va siguiendo al lider
	 * @param x posicion X del lider
	 * @param y posicion Y del lider
	 */
	public void moveCamera(float x, float y) {
		if (camera.position.x > x) {
			camera.position.x -= 1;
		} else if (camera.position.x < x) {
			camera.position.x += 1;
		}
		if (camera.position.y > y) {
			camera.position.y -= 1;
		} else if (camera.position.y < y) {
			camera.position.y += 1;
		}

	}
	/**
	 * Crea y a�ade dummies al array de dummies en lugares aleatorios del mapa
	 * @param dumbs numero de doomies que secrearan
	 */
	public void agregaDummies(int dumbs) {
		while (dummies.size() <= dumbs) {
			int col = (int) (Math.random() * columnas);
			int fil = (int) (Math.random() * filas);
			if (!mapa[col][fil].ocupado) {
				dummies.add(new Personaje(textDummie, mapa[col][fil]));
			}
		}
	}
	/**
	 * Agregamos los edificios con esta funcion. Comisarias, quemables y adyacentes.
	 */
	public void agregaEdificios() {

		edificios.add(new Edificio(mapa[0][8], 1, 1, true));
		
		Edificio escenario = new Edificio(mapa[1][3], 2, 1, true);
		ardiendo.add(escenario);
		edificios.add(escenario);
		edificios.add(new Edificio(mapa[1][28], 2, 3, false));
		edificios.add(new Edificio(mapa[1][24], 1, 3, false));
		edificios.add(new Edificio(mapa[1][20], 2, 3, false));
		comisaria1 = (new Edificio(mapa[1][17], 2, 2, true));
		edificios.add(comisaria1);
		edificios.add(new Edificio(mapa[1][13], 2, 3, false));
		edificios.add(new Edificio(mapa[1][10], 2, 2, false));
		edificios.add(new Edificio(mapa[1][5], 3, 2, true));
		
		edificios.add(new Edificio(mapa[2][8], 2, 2, false));
		
		edificios.add(new Edificio(mapa[3][23], 2, 3, false));
		edificios.add(new Edificio(mapa[3][14], 2, 2, false));
		edificios.add(new Edificio(mapa[3][10], 2, 2, false));
		edificios.add(new Edificio(mapa[3][3], 1, 1, true));
		
		edificios.add(new Edificio(mapa[4][29], 4, 2, true));
		edificios.add(new Edificio(mapa[4][21], 1, 1, true));
		edificios.add(new Edificio(mapa[4][19], 1, 1, true));
		edificios.add(new Edificio(mapa[4][17], 1, 1, true));
		edificios.add(new Edificio(mapa[4][27], 1, 1, true));

		edificios.add(new Edificio(mapa[5][10], 2, 3, false));
		edificios.add(new Edificio(mapa[5][4], 3, 2, false));
		edificios.add(new Edificio(mapa[5][7], 3, 2, false));
		edificios.add(new Edificio(mapa[5][1], 2, 2, false));
		
		edificios.add(new Edificio(mapa[6][27], 2, 1, false));
		edificios.add(new Edificio(mapa[6][24], 2, 2, true));
		edificios.add(new Edificio(mapa[6][20], 2, 3, false));
		edificios.add(new Edificio(mapa[6][18], 3, 1, false));
		edificios.add(new Edificio(mapa[6][14], 3, 3, true));
		
		edificios.add(new Edificio(mapa[7][14], 2, 3, false));

		
		edificios.add(new Edificio(mapa[8][10], 2, 3, false));
		edificios.add(new Edificio(mapa[8][1], 5, 2, true));
		
		edificios.add(new Edificio(mapa[9][30], 2, 1, false));
		edificios.add(new Edificio(mapa[9][27], 2, 2, false));
		edificios.add(new Edificio(mapa[9][23], 2, 3, false));
		edificios.add(new Edificio(mapa[9][20], 2, 2, false));
		
		edificios.add(new Edificio(mapa[10][17], 2, 3, false));
		edificios.add(new Edificio(mapa[10][14], 2, 2, false));
		edificios.add(new Edificio(mapa[10][4], 3, 2, false));
		edificios.add(new Edificio(mapa[10][7], 3, 2, false));

		edificios.add(new Edificio(mapa[11][20], 2, 2, false));
		edificios.add(new Edificio(mapa[11][10], 2, 3, false));
		
		edificios.add(new Edificio(mapa[12][30], 3, 1, true));
		edificios.add(new Edificio(mapa[12][27], 3, 2, true));
		edificios.add(new Edificio(mapa[12][24], 1, 2, false));
		edificios.add(new Edificio(mapa[12][23], 1, 1, true));
		edificios.add(new Edificio(mapa[12][17], 2, 3, false));
		edificios.add(new Edificio(mapa[12][15], 1, 1, true));
		edificios.add(new Edificio(mapa[12][14], 1, 1, true));
		
		
		edificios.add(new Edificio(mapa[13][18], 2, 3, false));
		edificios.add(new Edificio(mapa[13][11], 1, 1, true));
		edificios.add(new Edificio(mapa[13][12], 1, 1, true));
		edificios.add(new Edificio(mapa[13][15], 1, 1, true));

		
		edificios.add(new Edificio(mapa[14][25], 1, 1, true));
		edificios.add(new Edificio(mapa[14][23], 1, 1, false));
		edificios.add(new Edificio(mapa[14][22], 1, 1, true));
		edificios.add(new Edificio(mapa[14][11], 2, 3, false));
		edificios.add(new Edificio(mapa[14][17], 1, 1, true));
		edificios.add(new Edificio(mapa[14][7], 2, 3, false));
		edificios.add(new Edificio(mapa[14][3], 2, 3, true));
		edificios.add(new Edificio(mapa[14][0], 2, 1, false));
		edificios.add(new Edificio(mapa[14][15], 1, 1, true));
		edificios.add(new Edificio(mapa[14][1], 2, 1, true));
		
		edificios.add(new Edificio(mapa[16][30], 2, 2, false));
		edificios.add(new Edificio(mapa[16][26], 2, 3, false));
		edificios.add(new Edificio(mapa[16][22], 2, 3, true));
		edificios.add(new Edificio(mapa[16][18], 2, 3, false));
		edificios.add(new Edificio(mapa[16][15], 2, 2, true));
		
		edificios.add(new Edificio(mapa[17][8], 1, 2, false));
		edificios.add(new Edificio(mapa[17][6], 1, 1, true));
		edificios.add(new Edificio(mapa[17][3], 3, 2, true));
		edificios.add(new Edificio(mapa[17][1], 3, 1, false));
		edificios.add(new Edificio(mapa[17][11], 3, 3, false));

		edificios.add(new Edificio(mapa[18][30], 2, 2, false));
		edificios.add(new Edificio(mapa[18][19], 1, 2, false));
		
		comisaria2 =new Edificio(mapa[19][27], 2, 2, true);
		edificios.add(comisaria2);
		edificios.add(new Edificio(mapa[19][23], 2, 3, false));
		edificios.add(new Edificio(mapa[19][19], 2, 3, false));
		edificios.add(new Edificio(mapa[19][16], 2, 2, false));
		edificios.add(new Edificio(mapa[19][6], 1, 3, true));
		edificios.add(new Edificio(mapa[19][10], 1, 1, true));

		edificios.add(new Edificio(mapa[20][30], 2, 2, false));
		edificios.add(new Edificio(mapa[20][13], 2, 2, false));
		
		edificios.add(new Edificio(mapa[21][16], 2, 2, false));
		edificios.add(new Edificio(mapa[21][10], 2, 2, false));
		edificios.add(new Edificio(mapa[21][6], 2, 3, false));
		edificios.add(new Edificio(mapa[21][3], 2, 2, true));
		edificios.add(new Edificio(mapa[21][1], 2, 1, true));
		edificios.add(new Edificio(mapa[21][25], 2, 1, true));

		edificios.add(new Edificio(mapa[22][26], 2, 2, false));
		edificios.add(new Edificio(mapa[22][23], 1, 1, false));
		edificios.add(new Edificio(mapa[22][19], 5, 3, true));// PARLAMENTO
		edificios.add(new Edificio(mapa[22][23], 1, 1, true));

		edificios.add(new Edificio(mapa[23][30], 1, 1, true));
		edificios.add(new Edificio(mapa[23][27], 2, 2, false));
		edificios.add(new Edificio(mapa[23][15], 2, 3, false));
		edificios.add(new Edificio(mapa[23][13], 1, 1, true));
		
		edificios.add(new Edificio(mapa[24][13], 1, 1, false));
		edificios.add(new Edificio(mapa[25][13], 1, 1, true));
		edificios.add(new Edificio(mapa[24][23], 2, 2, false));
		edificios.add(new Edificio(mapa[24][9], 2, 3, false));
		comisaria3=new Edificio(mapa[24][6], 2, 2, true);
		edificios.add(comisaria3);
		edificios.add(new Edificio(mapa[24][4], 2, 1, true));//92
		edificios.add(new Edificio(mapa[24][1], 4, 2, true));
		
		edificios.add(new Edificio(mapa[25][25], 5, 1, true));
		edificios.add(new Edificio(mapa[25][30], 2, 2, false));
				
		edificios.add(new Edificio(mapa[26][23], 2, 2, false));
		edificios.add(new Edificio(mapa[26][16], 2, 2, false));
		edificios.add(new Edificio(mapa[26][14], 2, 1, true));
		edificios.add(new Edificio(mapa[26][27], 5, 2, true));

		edificios.add(new Edificio(mapa[27][30], 2, 2, false));
		edificios.add(new Edificio(mapa[27][12], 1, 1, true));
		edificios.add(new Edificio(mapa[27][10], 1, 1, false));
		edificios.add(new Edificio(mapa[27][4], 2, 5, false));
		
		edificios.add(new Edificio(mapa[28][18], 2, 3, false));
		edificios.add(new Edificio(mapa[28][22], 3, 3, false));

		
		edificios.add(new Edificio(mapa[29][16], 2, 1, false));
		edificios.add(new Edificio(mapa[29][13], 2, 2, false));
		edificios.add(new Edificio(mapa[29][9], 2, 3, false));
		edificios.add(new Edificio(mapa[29][1], 2, 3, false));
		
		edificios.add(new Edificio(mapa[30][19], 1, 2, false));
		edificios.add(new Edificio(mapa[30][18], 1, 1, true));
		edificios.add(new Edificio(mapa[30][30], 1, 1, true));
		edificios.add(new Edificio(mapa[30][7], 1, 1, true));
		edificios.add(new Edificio(mapa[30][6], 1, 1, false));
		edificios.add(new Edificio(mapa[30][5], 1, 1, true));

		//a�ade todos los edificios que toca a quemables
		for (int i = 0; i < edificios.size(); i++) {
			if (edificios.get(i).quemable) {
				quemables.add(edificios.get(i));
			}
		}
		//todos los edificios marcan sus casillas adyacentes
		for (int i = 0; i < quemables.size(); i++) {
			quemables.get(i).getAdy();
		}

	}

	/**
	 * Devuelve una casilla del mapa
	 * @param columna
	 * @param fila
	 * @return
	 */
	public static Casilla getMyCas(int columna, int fila) {
		Casilla xxx = null;
		try {
			xxx = mapa[fila][columna];
		} catch (Exception e) {
			// TODO
		}
		return xxx;
	}

	/** Devuelve las medidas del mapa. Columnas, Filas */
	public static int[] lados() {
		int[] medidas = { columnas, filas };
		return medidas;
	}
	/**
	 * a�ade policias a polis y le da posicion de una comisaria
	 * @param num
	 */
	public void agregaPolis(int num){
		for(int i = 0 ; i < num ; i++){
			if(polis.size()%3==0 || i==0){
				polis.add(new Madero(comisaria1.casillaInicial));
			}else if(polis.size()%3==1 || i==1){
				polis.add(new Madero(comisaria2.casillaInicial));
			}else if(polis.size()%3==2 || i==2){
				polis.add(new Madero(comisaria3.casillaInicial));
			}
		}
	}
	/**
	 * recorre todos los edificios quemables y comprueba si estan rodeados.
	 * En tal caso los a�ade a ardiendo y los borra de quemables.
	 */
	public void compruebaQuemables(){
		for (int i = 0; i < quemables.size(); i++) {
			if (quemables.get(i).isRodeado()) {
				//quemables.get(i).quemado = true;
				ardiendo.add(quemables.get(i));
				game.sumaPuntos(quemables.get(i).casillasAlto*quemables.get(i).casillasAlto*100);
				quemables.remove(quemables.get(i));


			}
		}
	}
	/**
	 * marca los adyacentes de un edificio
	 * @param edif
	 */
	public void pintalo(Edificio edif){
		for (int dum = 0; dum < edif.adyacentes.size(); dum++) {
			game.batch.draw(textFire,
					edif.adyacentes.get(dum).pixelX,
					edif.adyacentes.get(dum).pixelY, 32, 32);
		}
	}
	/**
	 * Pinta sobre el mapa el radio de vision de un policia.
	 * @param poli
	 */
	public void poliVision(Madero poli){
		for (int dum = 0; dum < poli.campoVision.size(); dum++) {
			game.batch.draw(textPoliVision,
					poli.campoVision.get(dum).pixelX,
					poli.campoVision.get(dum).pixelY, 32, 32);
		}
	}
	/**
	 * Funcion para visualizar las posiciones proximas de un poli
	 * @param poli
	 */
	public void poliMov(Madero poli){
		    Enumeration casillas = poli.dirs.keys();
			while(casillas.hasMoreElements()) { 
				Casilla kaz = (Casilla) casillas.nextElement();
				game.batch.draw(movimiento,
						kaz.pixelX,
						kaz.pixelY, 32, 32);
			} 
			
		
	}
	public void pintaloXCas(Casilla cas){ 
		for(int du = 0; du < edificios.size(); du++) {
			if(edificios.get(du).adyacentes.contains(cas)){
			
				Edificio edif = edificios.get(du);
				for (int dum = 0; dum < edif.adyacentes.size(); dum++) {
					game.batch.draw(textFire,
							edif.adyacentes.get(dum).pixelX,
							edif.adyacentes.get(dum).pixelY, 32, 32);
				}
			}else{
				System.out.println("edificio no encontrado");
			}
		}
		
	}
	/**
	 * Compara la casilla del lider con la de todos los dummies y si coincide lo a�ade a la manifstacion
	 */
	public void nuevoManifestante(){
		for (int i = 0; i < dummies.size(); i++) {
			if (dummies.get(i).casilla.equals(manifa.get(0).casilla)) {
				dummies.get(i).texturaPj = textFollower;
				manifa.add(dummies.get(i));
				dummies.remove(dummies.get(i));
				game.sumaPuntos(100);
			}

		}
	}
	/**
	 * Marca las casillas como manifestadas
	 */
	public void manifestaCasillas(){
		//marca todas las casillas como des-manifestadas
		for (int i = 0; i < filas; i++) {
			for (int j = 0; j < columnas; j++) {
				mapa[j][i].manifestado = false;
			}
		}
		//marca las casillas de manifestantes como manifestadas
		for (int k = 0; k < manifa.size(); k++) {
			manifa.get(k).casilla.manifestado = true;
		}
	}
	/**
	 * Mueve todos los la manifestacion
	 * @param auxCasilla
	 * @return auxCasilla
	 */
	public void mueveMani(){
		Casilla aux2 = null;
		for (int i = 1; i < manifa.size(); i++) {
			if (i == 1) {
				aux2 = manifa.get(i).casilla;
				manifa.get(i).casilla = auxCasilla;
			} else {
				if (i % 2 == 0) {
					auxCasilla = manifa.get(i).casilla;
					manifa.get(i).casilla = aux2;
				} else {
					aux2 = manifa.get(i).casilla;
					manifa.get(i).casilla = auxCasilla;
				}
			}
		}
	}
	/**
	 * Controla si el lider esta en una casilla de metro y si es asi lo transprta a la otra salida de metro
	 */
	public void cogeMetro(){
		if (manifa.get(0).casilla.equals(l1a)) {
			manifa.get(0).casilla = l1b;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);

		} else if (manifa.get(0).casilla.equals(l1b)) {
			manifa.get(0).casilla = l1a;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);
		}
		if (manifa.get(0).casilla.equals(l2a)) {
			manifa.get(0).casilla = l2b;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);

		} else if (manifa.get(0).casilla.equals(l2b)) {
			manifa.get(0).casilla = l2a;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);
		}
		if (manifa.get(0).casilla.equals(l3a)) {
			manifa.get(0).casilla = l3b;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);

		} else if (manifa.get(0).casilla.equals(l3b)) {
			manifa.get(0).casilla = l3a;
								camera.position.set(manifa.get(0).casilla.pixelX,
										manifa.get(0).casilla.pixelY, 0);
		}
	}
	private void pauseListener(boolean isPaused){
		if (Gdx.input.isKeyPressed(Keys.P)){

			if(isPaused) {

				paused = false;
			} else {

				paused = true;
			}
		}

	}
	/**
	 * 
	 */
	public boolean pauseListener(){
		if (Gdx.input.isKeyPressed(Keys.P)){
			if(paused){
				game.resume();
				paused=false;
				
				
			}else{
				game.pause();
				paused=true;
			}
		}
		return paused;
	}
	/**
	 * Escucha si se ha pulsado alguna de las flechas y cambia la posicion del lider
	 * @param auxCasilla
	 * @return
	 */
	public void teclasListener(){
		if (Gdx.input.isKeyPressed(Keys.W)
				&& (manifa.get(0).casilla.fila + 1 < filas)
				&& !mapa[manifa.get(0).casilla.columna][manifa.get(0).casilla.fila + 1].ocupado) {
			if(!moved){
				auxCasilla = manifa.get(0).casilla;
				manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna][manifa
				                                                            .get(0).casilla.fila + 1];
				moved = true;
				lider.direccion="up";
				

			}}
		/* ABAJO */
		if (Gdx.input.isKeyPressed(Keys.S)
				&& (manifa.get(0).casilla.fila - 1 >= 0)
				&& !mapa[manifa.get(0).casilla.columna][manifa.get(0).casilla.fila - 1].ocupado) {
			if(!moved){

				auxCasilla = manifa.get(0).casilla;
				manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna][manifa
				                                                            .get(0).casilla.fila - 1];
				moved = true;
				lider.direccion="down";

			}}
		/* DERECHA */
		if (Gdx.input.isKeyPressed(Keys.D)
				&& (manifa.get(0).casilla.columna + 1 < columnas)
				&& !mapa[manifa.get(0).casilla.columna + 1][manifa.get(0).casilla.fila].ocupado) {
			if(!moved){

				auxCasilla = manifa.get(0).casilla;
				manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna + 1][manifa
				                                                                .get(0).casilla.fila];
				moved = true;
				lider.direccion="right";

			}}
		/* IZQUIERDA */
		if (Gdx.input.isKeyPressed(Keys.A)
				&& (manifa.get(0).casilla.columna - 1 >= 0)
				&& !mapa[manifa.get(0).casilla.columna - 1][manifa.get(0).casilla.fila].ocupado) {
			if(!moved){

				auxCasilla = manifa.get(0).casilla;
				manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna - 1][manifa
				                                                                .get(0).casilla.fila];
				moved = true;
				lider.direccion="left";
			}
		}
		//manifa.get(0).texturaPj=lider.cambiaTextura();
	}
	/**
	 * Escucha si se ha pulsado alguna de las flechas y cambia la posicion del lider
	 * @param auxCasilla 
	 * @return auxCasilla
	 */
	public void flechasListener(){
		if (Gdx.input.isKeyPressed(Keys.UP)
				&& (manifa.get(0).casilla.fila + 1 < filas)
				&& !mapa[manifa.get(0).casilla.columna][manifa.get(0).casilla.fila + 1].ocupado) {
			auxCasilla = manifa.get(0).casilla;
			manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna][manifa
			                                                            .get(0).casilla.fila + 1];
			moved = true;

		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)
				&& (manifa.get(0).casilla.fila - 1 >= 0)
				&& !mapa[manifa.get(0).casilla.columna][manifa.get(0).casilla.fila - 1].ocupado) {
			auxCasilla = manifa.get(0).casilla;
			manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna][manifa
			                                                            .get(0).casilla.fila - 1];
			moved = true;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)
				&& (manifa.get(0).casilla.columna + 1 < columnas)
				&& !mapa[manifa.get(0).casilla.columna + 1][manifa.get(0).casilla.fila].ocupado) {
			auxCasilla = manifa.get(0).casilla;
			manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna + 1][manifa
			                                                                .get(0).casilla.fila];
			moved = true;
		}
		if (Gdx.input.isKeyPressed(Keys.LEFT)
				&& (manifa.get(0).casilla.columna - 1 >= 0)
				&& !mapa[manifa.get(0).casilla.columna - 1][manifa.get(0).casilla.fila].ocupado) {
			auxCasilla = manifa.get(0).casilla;
			manifa.get(0).casilla = mapa[manifa.get(0).casilla.columna - 1][manifa
			                                                                .get(0).casilla.fila];
			moved = true;
		}
	}
	
//	public float[] transicion(double vel,Casilla origen,Casilla destino){
//		float dX=destino.pixelX,dY=destino.pixelY,oX=origen.pixelX,oY=origen.pixelY;
//		float miniTama�o = tamano/10;
//		game.batch.draw(lider.texturaPj, lider.casilla.pixelX+transicion(),
//				lider.casilla.pixelY+transicion(), 32, 32);
//		
//		return null;
//		
//	}
}
