package com.me.borntoburn.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.me.borntoburn.BaseScreen;
import com.me.borntoburn.Drop;

public class SplashScreen extends BaseScreen {
	private Drop propiedades;
	private float tempsAcumulat = 0;
	Texture logo;

	public SplashScreen(Drop game) {
		this.propiedades = game;

		// Hide cursor
		Gdx.input.setCursorCatched(true);
		// logo = new Texture(Gdx.files.internal("data/SplashLogo.png"));

		game.manager.load("data/img/amarillo.png", Texture.class);
		game.manager.load("data/img/azul.png", Texture.class);
		game.manager.load("data/img/rojo.png", Texture.class);
		game.manager.load("data/img/verde.png", Texture.class);
		
		
		//game.manager.load("data/img/heavy.png", Texture.class);
		game.manager.load("data/img/people.png", Texture.class);
		game.manager.load("data/img/fireSprite.png", Texture.class);
		game.manager.load("data/img/followers.png", Texture.class);
		game.manager.load("data/img/prado.png", Texture.class);
		game.manager.load("data/img/exit.png", Texture.class);
		game.manager.load("data/img/replay.png", Texture.class);
		game.manager.load("data/img/continue.png", Texture.class);
		// game.manager.load("data/img/main0.png", Texture.class);
		// game.manager.load("data/img/main1.png", Texture.class);
		// game.manager.load("data/img/main2.png", Texture.class);
		game.manager.load("data/sound/ost.mp3", Music.class);
		game.font.setColor(Color.BLACK);
	}

	@Override
	public void render(float delta) {
		super.render(delta);

		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		propiedades.batch.begin();

		if (propiedades.manager.update() && tempsAcumulat > 2) {
			Gdx.app.log("Splash", "Cargado!!");
			dispose();
			propiedades.setScreen(new MainMenuScreen(propiedades));
		} else {
			tempsAcumulat += delta;
			propiedades.font.draw(propiedades.batch, "Loading  "
					+ (int) (propiedades.manager.getProgress() * 100) + " %",
					(propiedades.ancho/2)-(70), (propiedades.alto/2));
			Gdx.app.log("Splash",
					"No carregat: " + propiedades.manager.getProgress() * 100
							+ " %");
		}

		// game.batch.draw(logo, game.w / 2 - logo.getWidth() / 2, game.h / 2
		// - logo.getHeight() / 2);

		propiedades.batch.end();
	}

	@Override
	public void dispose() {
		super.dispose();

	}
}
