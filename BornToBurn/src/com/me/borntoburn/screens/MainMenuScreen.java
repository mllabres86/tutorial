package com.me.borntoburn.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.me.borntoburn.Drop;

public class MainMenuScreen implements Screen {

	final Drop game;
	Texture[] texts = new Texture[3];
	OrthographicCamera camera;

	public MainMenuScreen(final Drop gam) {
		// Hide cursor
		Gdx.input.setCursorCatched(false);
		game = gam;
		// logo = new Texture(Gdx.files.internal("data/logo.png"));
		camera = new OrthographicCamera();
		camera.setToOrtho(false, game.ancho, game.alto);
		// for (int i = 0; i < 3; i++) {
		// texts[i] = game.manager.get("data/img/main" + i + ".png",
		// Texture.class);
		//
		// }
		Gdx.app.log("main menu", "Ancho: " + game.ancho + " alto: " + game.alto);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b,
				Color.WHITE.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		game.batch.setProjectionMatrix(camera.combined);
		game.font.setColor(Color.BLACK);
		game.batch.begin();

		// game.batch.draw(logo, logo.getWidth() / 2, logo.getHeight() / 2);

		game.batch.end();

		if (Gdx.input.isKeyPressed(Keys.ENTER)) {
			Gdx.app.log("main menu", "Entrando juego");
			game.setScreen(new BornToBurn(game));
			dispose();
		}

		if (Gdx.input.isKeyPressed(Keys.Q)) {
			Gdx.app.exit();
			dispose();
		}

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		System.out.println("its paused!!!!");
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
